"""Stream type classes for tap-fixerio."""

from singer_sdk import typing as th

from tap_fixerio.client import FixerioStream


class CurrencyStream(FixerioStream):
    """Define custom stream."""

    name = "rates"
    path = "timeseries"
    primary_keys = ["rates"]
    replication_key = "date"
    schema = th.PropertiesList(
        th.Property("date", th.DateType),
        th.Property("base", th.StringType),
        th.Property("rates", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()
